package com.example.celsiustofahrenheit;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private Converter convert;

    public void init() {
        convert = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//hello
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Converted Temperature";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius Temperature: " +
                request.getParameter("tmp") + "\n" +
                "  <P>Fahrenheit Temperature: " +
                convert.convertedTemperature(Double.parseDouble(request.getParameter("tmp"))) +
                "</BODY></HTML>");
    }

    public void destroy() {
    }
}